var mongoose = require('mongoose');
mongoose.Promise = require('bluebird');

var Question = require('./models/Question.js');

var uristring =
	process.env.MONGODB_URI ||
	'mongodb://localhost/quiz';

mongoose.connect(uristring, function(err, res){

	if (err) {
		console.log ('ERROR connecting to: ' + uristring + '. ' + err);
	} else {
		console.log ('Succeeded connected to: ' + uristring);
	}

	mongoose.connection.db.dropDatabase(); // Clean Up

	// https://www.w3schools.com/quiztest/quiztest.asp?qtest=JavaScript
	var questions = [
	{ // 1
		"title": "Inside which HTML element do we put the JavaScript?",
		"answers": [
			{ "is_correct": true, "text": "<script>" },
			{ "is_correct": false, "text": "<scripting>" },
			{ "is_correct": false, "text": "<js>" },
			{ "is_correct": false, "text": "<javascript>" }
		]
	},
	{ // 2
		"title": "What is the correct JavaScript syntax to change the content of the HTML element? <p id='demo'>This is a demonstration.</p>",
		"answers": [
			{ "is_correct": false, "text": "#demo.innerHTML = 'Hello World!';" },
			{ "is_correct": false, "text": "document.getElement('p').innerHTML = 'Hello World!';" },
			{ "is_correct": false, "text": "document.getElementByName('p').innerHTML = 'Hello World!';" },
			{ "is_correct": true, "text": "document.getElementById('demo').innerHTML = 'Hello World!';" }
		]
	},
	{ // 3
		"title": "Where is the correct place to insert a JavaScript?",
		"answers": [
			{ "is_correct": true, "text": "The <head> section" },
			{ "is_correct": true, "text": "The <body> section" },
			{ "is_correct": false, "text": "After <body> section" },
			{ "is_correct": true, "text": "Both the <head> section and the <body> section are correct" }
		]
	},
	{ // 4
		"title": "What is the correct syntax for referring to an external script called 'xxx.js'?",
		"answers": [
			{ "is_correct": false, "text": "<script name='xxx.js'>" },
			{ "is_correct": true, "text": "<script src='xxx.js'>" },
			{ "is_correct": false, "text": "<script href='xxx.js'>" }
		]
	},
	{ // 5
		"title": "The external JavaScript file must contain the <script> tag.",
		"answers": [
			{ "is_correct": true, "text": "False" },
			{ "is_correct": false, "text": "True" },
			{ "is_correct": false, "text": "It will work either way." }
		]
	},
	{ // 6
		"title": "How do you write 'Hello World' in an alert box?",
		"answers": [
			{ "is_correct": false, "text": "msgBox('Hello World');" },
			{ "is_correct": false, "text": "alertBox('Hello World');" },
			{ "is_correct": true, "text": "alert('Hello World');" },
			{ "is_correct": false, "text": "msg('Hello World');" }
		]
	},
	{ // 7
		"title": "How do you create a function in JavaScript?",
		"answers": [
			{ "is_correct": true, "text": "function myFunction()" },
			{ "is_correct": false, "text": "function = myFunction()" },
			{ "is_correct": false, "text": "function:myFunction()" }
		]
	},
	{ // 8
		"title": "How do you call a function named 'myFunction'?",
		"answers": [
			{ "is_correct": false, "text": "call myFunction()" },
			{ "is_correct": true, "text": "myFunction()" },
			{ "is_correct": false, "text": "call function myFunction()" }
		]
	},
	{ // 9
		"title": "How to write an IF statement in JavaScript?",
		"answers": [
			{ "is_correct": false, "text": "if i == 5 then" },
			{ "is_correct": true, "text": "if (i == 5)" },
			{ "is_correct": false, "text": "if i = 5" },
			{ "is_correct": false, "text": "if i = 5 then" }
		]
	},
	{ // 10
		"title": "How to write an IF statement for executing some code if 'i' is NOT equal to 5?",
		"answers": [
			{ "is_correct": false, "text": "if i =! 5 then" },
			{ "is_correct": false, "text": "if i <> 5" },
			{ "is_correct": true, "text": "if (i != 5)" },
			{ "is_correct": false, "text": "if (i <> 5)" }
		]
	}
	];

	questions.forEach(function(item){
		new Question(item).save();
	})
})
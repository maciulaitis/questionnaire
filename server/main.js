var express = require('express');

var app = new express();

var parser = require('body-parser');

require('./database.js');

app.get("/", function(req, res) {
  res.render('./../app/index.ejs', {});
})
.use(express.static(__dirname + './../build'))

// if(!module.parent){/*
var server = app.listen((process.env.PORT || 7777), false, function () {
	 var host = server.address().address;
	 var port = server.address().port;
	 console.log('Server listening at http://%s:%s', host, port);
});
// }

app.use(parser.json());
app.use(parser.urlencoded({ extended: false }));

require('./routes/items.js')(app); // /api/items

module.exports = server;
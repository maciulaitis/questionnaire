var mongoose = require('mongoose');

var QuestionSchema = {
	id: String,
	title: String,
	answers: [{
        text: String,
        is_correct: Boolean
    }]
};

var  Question;

try {
  	Question = mongoose.model('Question');
} catch (error) {
	Question = mongoose.model('Question', QuestionSchema, 'questions');
}

module.exports = Question;
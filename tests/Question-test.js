//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

let mongoose = require("mongoose");
let database = require('./../server/database.js');
let Question = require('./../server/models/Question.js');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('./../server/main');
let should = chai.should();

chai.use(chaiHttp);
//Our parent block
describe('API', () => {
    // beforeEach((done) => { //Before each test we empty the database
    //   Question.remove({}, (err) => { 
    //     done();
    //   });
    // });
  /*
  * Test the /GET route
  */
  it('should list ALL items on /api/items GET', function(done) {
    chai.request(server)
      .get('/api/items')
      .end(function(err, res){
          res.should.have.status(200);
          res.body.should.be.a('array');
          res.body.length.should.be.eql(10);
        done();
      });
  });

});

describe('DB', () => {
    beforeEach((done) => { //Before each test we empty the database
        Question.remove({}, (err) => { 
           done();         
        });

        var item = { // 10
          "title": "How to write an IF statement for executing some code if 'i' is NOT equal to 5?",
          "answers": [
            { "is_correct": false, "text": "if i =! 5 then" },
            { "is_correct": false, "text": "if i <> 5" },
            { "is_correct": true, "text": "if (i != 5)" },
            { "is_correct": false, "text": "if (i <> 5)" }
          ]
        };

        let q = new Question(item).save();

    });
  /*
  * Test the /GET route
  */
  it('should have ONE item on /api/items GET', function(done) {
    chai.request(server)
      .get('/api/items')
      .end(function(err, res){
          res.should.have.status(200);
          res.body.should.be.a('array');
          res.body.length.should.be.eql(1);
        done();
      });
  });

});

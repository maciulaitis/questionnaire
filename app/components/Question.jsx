var React = require('react');
var PropTypes = require('prop-types');
var createReactClass = require('create-react-class');

var Answer = require('./Answer.jsx');

var Question = createReactClass({
  propTypes: {
    setAnswer: PropTypes.func,
    validateAnswers: PropTypes.func,
    reviewAnswers: PropTypes.func,
    data: PropTypes.object
  },

  render: function(){
    var answersNodes = Object.keys(this.props.data.answers).map(function(value, index){
      return (
        <Answer
          id={index}
          key={"item"+index}
          value={value}
          data={this.props.data.answers[index]}
          setAnswer={this.props.setAnswer}/>
      )
    }.bind(this));

    return (
      <div>
        <h4>{(parseInt(this.props.id) + 1) + ": " + this.props.data.title}</h4>
        <form>
          {answersNodes}
          <br/>
          <button type="button" onClick={this.props.validateAnswers}>Validate answer</button>
        </form>
      </div>
    );
  }
});

module.exports = Question;

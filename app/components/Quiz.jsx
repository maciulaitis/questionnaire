var $ = require('jquery');
var React = require('react');
var createReactClass = require('create-react-class');

var Question = require('./Question.jsx');

var Quiz = createReactClass({

  getInitialState: function(){
    return {
      quiz: {
        title: this.props.title,
      },
      user_answers: [],
      step: 0
    }
  },

  componentDidMount: function(){
    console.log(this.state.quiz);
  },

  nextStep: function(){
    if(!this.state.user_answers[this.state.step]) {
      alert('Please answer the question'); // Print some error msg
    } else {
      this.setState({step: (this.state.step + 1)});
    }
  },

  tryAgain: function() {
    this.setState({step: 0, user_answers: []});
  },

  setAnswer: function(event){
    this.state.user_answers[this.state.step] = this.state.user_answers[this.state.step] || [];
    this.state.user_answers[this.state.step][parseInt(event.target.value)] = event.target.checked;
  },

  isAnswerRight: function(index){
    var result = true;
    Object.keys(this.props.items[index].answers).map(function(value, answer_index){
      var answer = this.props.items[index].answers[value];

      if (!this.state.user_answers[index] || (answer.is_correct != (this.state.user_answers[index][value] || false))) {
        result = false;
      }
    }.bind(this));
    return result;
  },

  computeScore: function(index){
    var score = 0
    Object.keys(this.props.items).map(function(value, index){
      if (this.isAnswerRight(parseInt(value))) {
        score = score + 1;
      }
    }.bind(this));
    return score;
  },

  renderResult: function(){
    var result = Object.keys(this.props.items).map(function(value, index){
      if (this.isAnswerRight(value)) {
        return (
          <div key={"q"+index} className="correct-answer">{"Question " + (index+1) + ": You were right!"}</div>
        )
      } else {
        return (
          <div key={"q"+index} className="wrong-answer">{"Question " + (index+1) + ": You were wrong!"}</div>
        )
      }
    }.bind(this));
    return (
      <div>
        <h3>Results</h3>
        <div>
          {this.computeScore()}/{this.props.items.length}
        </div>
        <div>
          <h3>Your answers</h3>
            {result}
        </div>

        <br/>
        <button type="button" onClick={this.tryAgain}>Try again</button>
      </div>
   );
  },

  render: function(){
    if (!this.props.items) {return <div></div>}
    return (
      <div className="twelve columns">
        <h1>{this.state.quiz.title}</h1>
        {(this.state.step < this.props.items.length
          ? (<Question
                id={this.state.step}
                key={this.state.step}
                data={this.props.items[this.state.step]}
                validateAnswers={this.nextStep}
                setAnswer={this.setAnswer}/>)
          : (<div>{this.renderResult()}</div>)
        )}
      </div>
    )
  }
});

module.exports = Quiz;

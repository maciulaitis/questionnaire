var React = require('react');
var createReactClass = require('create-react-class');

var Answer = createReactClass({
  render: function(){
    return (
      <div className="row">
        <div className="one column">
          <input
            id={"answer-input-" + this.props.id}
            type="checkbox"
            value={this.props.value}
            onChange={this.props.setAnswer}
            defaultChecked={false}
          />
        </div>
        <div className="eleven columns">
          <label htmlFor={"answer-input-" + this.props.id}>
            {this.props.data.text}
          </label>
        </div>
      </div>
    )
  }
})

module.exports = Answer;

var helper = require('./../helpers/RestHelper.js');

function QuizStore() {

  var items = [];

  helper.get("api/items") // get questions
  .then(function(data){
    items = data;
    triggerListeners();
  })

  var listeners = [];

  function getItems() {
    return items;
  }

  function onChange(listener) {
    listeners.push(listener);
  }

  function triggerListeners() {
    listeners.forEach(function(listener) {
      listener(items);
    })
  }

  return {
    getItems: getItems,
    onChange: onChange
  }
}

module.exports = new QuizStore();

var React = require('react');
var ReactDom = require('react-dom');

var Quiz = require('./components/Quiz.jsx');

var quizStore = require('./stores/QuizStore.jsx');
var initial = quizStore.getItems();

function render() {
  ReactDom.render(<Quiz items={initial} title={"Questionnaire"}/>, app);
}

quizStore.onChange(function(items) {
  initial = items;
  render();
})

render();
